```mermaid
sequenceDiagram

participant User
participant User Service
participant Billing Service
participant Notification Service
	
User->>User Service: POST /users/register
activate User Service
%% Note right of User Service: создание акккаунта в биллинге
User Service->>Billing Service: POST /accounts {userId}
activate Billing Service
Billing Service-->>User Service: 201 CREATED {accountId}
deactivate Billing Service
User Service-->>User: 201 CREATED
deactivate User Service
User->>Notification Service: GET /messages{userId}
activate Notification Service
Notification Service -->> User: 202 ACCEPTED
deactivate Notification Service
User->>Billing Service: POST /order/{orderId,userId,amount}
activate Billing Service
Billing Service->>Notification Service: POST /send_email {template_id, email, context}
activate Notification Service
Notification Service -->> Billing Service: 202 ACCEPTED
Billing Service -->> User: 201 CREATED
deactivate Billing Service
Notification Service ->> Notification Service: sending email
deactivate Notification Service
```
